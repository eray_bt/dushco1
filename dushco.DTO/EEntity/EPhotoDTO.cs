﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dushco.DTO.EEntity
{
    public class EPhotoDTO
    {
        public string PhotoName { get; set; }
        public string PhotoURL { get; set; }
        public string Photo360 { get; set; }
    }
}
