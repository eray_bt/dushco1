﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dushco.DTO.EEntity
{
    public class EVideoDTO
    {
        public string VideoName { get; set; }
        public string YouTubeUrl { get; set; }
    }
}
