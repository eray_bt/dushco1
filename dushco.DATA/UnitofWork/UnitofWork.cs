﻿using dushco.DATA.Context;
using dushco.DATA.GenericRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dushco.DATA.UnitofWork
{
    public class UnitofWork :IUnitofWork
    {
        private readonly DushcoContext _context;
        private bool dispose = false;
        DbContextTransaction transaction;

        public UnitofWork(DushcoContext context)
        {
            Database.SetInitializer<DushcoContext>(null);
            if (context==null)
            {
                throw new ArgumentException("Context is Null");
            }
            _context = context;
        }
        public IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity :class
        {
            return new GenericRepository<TEntity>(_context);
        }
        public void BeginTransaction()
        {
            transaction = _context.Database.BeginTransaction();
        }
        public void Commit()
        {
            transaction.Commit();
        }
        public void RollBack()
        {
            transaction.Rollback();
        }
        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
        public virtual void Dispose(bool disposing)
        {
            if (!this.dispose)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.dispose = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
