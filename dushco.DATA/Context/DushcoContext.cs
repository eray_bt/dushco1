﻿using dushco.CORE.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dushco.DATA.Context
{
    public class DushcoContext :DbContext
    {
        private readonly DushcoContext _context;

        public DushcoContext():base("name=dushcoDB")
        {
            Configuration.LazyLoadingEnabled=false;
        }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<Photo> Photo { get; set; }
        public virtual DbSet<Slider> Slider { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Video> Video { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().ToTable("Category","dbo");
            modelBuilder.Entity<News>().ToTable("News", "dbo");
            modelBuilder.Entity<Photo>().ToTable("Photo", "dbo");
            modelBuilder.Entity<Slider>().ToTable("Slider", "dbo");
            modelBuilder.Entity<User>().ToTable("User", "dbo");
            modelBuilder.Entity<Video>().ToTable("Video", "dbo");
            base.OnModelCreating(modelBuilder);
        }

    }
}
