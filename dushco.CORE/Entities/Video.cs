﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dushco.CORE.Entities
{
    public class Video:Base
    {
        public string VideoName { get; set; }
        public string YouTubeUrl { get; set; }
    }
}
