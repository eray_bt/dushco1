﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dushco.CORE.Entities
{
    public class News :Base
    {
        public string EN_Title { get; set; }
        public string RU_Title { get; set; }
        public string AR_Title { get; set; }
        public string EN_Description { get; set; }
        public string RU_Description { get; set; }
        public string AR_Description { get; set; }
        public DateTime FairDate { get; set; }
        public bool FairStatus { get; set; }
    }
}
