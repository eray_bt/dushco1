using dushco.CORE.Entities;
using dushco.DATA.GenericRepository;
using dushco.DATA.UnitofWork;
using dushco.SERVICES.Services;
using System.Web.Mvc;
using Unity;
using Unity.Lifetime;
using Unity.Mvc5;

namespace dushco.IoC
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
        public static void RegisterTypes(IUnityContainer container)
        {
            container.BindInRequestScope<IUnitofWork, UnitofWork>();
            container.BindInRequestScope<ICategoryServices, CategoryServices>();
            //container.BindInRequestScope<INewService, NewsService>();
            //container.BindInRequestScope<IPhotoService, PhotoService>();
            //container.BindInRequestScope<ISliderService, SliderService>();
            //container.BindInRequestScope<IUserService>,UserService();
            //container.BindInRequestScope<IVideoService, VideoService>();
            container.BindInRequestScope<IGenericRepository<Category>, GenericRepository<Category>>();
            container.BindInRequestScope<IGenericRepository<News>, GenericRepository<News>>();
            container.BindInRequestScope<IGenericRepository<Photo>, GenericRepository<Photo>>();
            container.BindInRequestScope<IGenericRepository<Slider>, GenericRepository<Slider>>();
            container.BindInRequestScope<IGenericRepository<User>, GenericRepository<User>>();
            container.BindInRequestScope<IGenericRepository<Video>, GenericRepository<Video>>();
        }
        public static void BindInRequestScope<T1,T2>(this IUnityContainer container)where T2:T1
        {
            container.RegisterType<T1, T2>(new HierarchicalLifetimeManager());
        }
    }
}
